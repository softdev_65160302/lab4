/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;


/**
 *
 * @author informatics
 */
public class Game {
    private Table table;
    private Player player1,player2;
    
    public Game(){
        player1 = new Player("X");
        player2 = new Player("O");
    }
    public void play(){
        showWelcome();
        newGame();
        while(true){
        showTable();
        showTurn();
        inputRowCol();
        if(table.checkWin()){
            showTable();
            System.out.println(table.getCurrentPlayer().getSymbol()+" Winner!!!");
            saveStat();
            showStat();
            if (continueGame()){
                newGame();
                continue;
            }else{
                System.out.println("<<<<< Thank You For Playing >>>>>");
                break;
            }
        }else if(table.checkDraw()){
            showTable();
            System.out.println("Draw!!!");
            saveStat();
            showStat();
            if (continueGame()){
                newGame();
                continue;
            }else{
                break;
            }
        }
        table.switchPlayer();
        }
    }

    private void showWelcome() {
        System.out.println("<<<<< Welcome to OX Game >>>>>");
    }

    private void showTable() {
      String[][] tab =  table.getTable();
        for (int i = 0; i < 3; i++) {
           for (int j = 0;j < 3;j++){
               System.out.print(tab[i][j]+" ");
            }
            System.out.println();
        }
    }

    private void newGame() {
       table = new Table(player1, player2);
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        int row = kb.nextInt();
        int col = kb.nextInt();
        table.setRowCol(row, col);
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }
    
    private void saveStat(){
        if (table.getCurrentPlayer().getSymbol() == "X"){
            player1.win();
            player2.lose();
        }else if(table.getCurrentPlayer().getSymbol() == "O") {
            player2.win();
            player1.lose();
        }else{
            player1.draw();
            player2.draw();
        }
    }
    
    private void showStat(){
        System.out.println("<<<<<Stat History>>>>>");
        System.out.println("Player 1 WIN: "+player1.getWinCount());
        System.out.println("Player 1 LOSE: "+player1.getLoseCount());
        System.out.println("Player 1 DRAW: "+player1.getDrawCount());
        System.out.println("Player 2 WIN: "+player2.getWinCount());
        System.out.println("Player 2 LOSE: "+player2.getLoseCount());
        System.out.println("Player 2 DRAW: "+player2.getDrawCount());
    }
    
    static boolean continueGame() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Continue(Y/N)?:");
        String ans = kb.next();
        if("Y".equals(ans)) {
            return true;
        }else{
            return false;
        }
    }
}
