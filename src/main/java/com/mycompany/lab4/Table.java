/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;


/**
 *
 * @author informatics
 */
public class Table {

    private String[][] table = {{"-", "-", "-"},
    {"-", "-", "-"},
    {"-", "-", "-"}};
    private Player currentPlayer;
    private Player player1;
    private Player player2;
    private int row, col;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public String[][] getTable() {
        return table;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] != "-") {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        this.row = row;
        this.col = col;
        return true;
    }

    public boolean checkWin() {
        if(checkRow()) {
            return true;
        }else if(checkCol()){
            return true;
        }else if(checkDiagonalRight()){
            return true;
        }else if(checkDiagonalLeft()){
            return true;
        }
        return false;
    }
  
    public boolean checkDraw(){
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if (table[i][j] == "-") {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonalRight() {
        for (int i = 0,j = 0; i < 3; i++, j++) {
            if (table[i][j] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
    
    public boolean checkDiagonalLeft(){
        for(int i = 0,j = 2; i < 3; i++, j--){
            if(table[i][j] != currentPlayer.getSymbol()){
                return false;
            }
        }
        return true;
    }
    
    Player getCurrentPlayer() {
        return currentPlayer;
    }

    void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;

        } else {
            currentPlayer = player1;
        }
    }
}
